Tag: patch-not-forwarded-upstream
Severity: info
Check: debian/patches/dep3
Renamed-From: send-patch
Explanation: According to the DEP-3 headers, this patch has not been forwarded
 upstream. Please forward the patch upstream and work with them to ensure
 the patch is included in the version control system and in the next
 upstream release of your package.
 .
 If this patch should not be forwarded upstream please put not-needed in
 the Forwarded header.
See-Also: social contract item 2,
 devref 3.1.4,
 policy 4.3,
 Bug#755153
